¿Que es hardware?
R= conjunto de elementos fisicos o materiales que constituyen una computadora o un sistema.

¿Que es software?
R=Conjunto de programas y rutinas que permiten a la computadora realizar determinadas tareas.

¿Que es un IDE de programacion?
R= Es un entorno de programacion que ha sido empaquetado como un programa de aplicacion , consiste en:
Editor de codigo, compilador, depurador y constructor de interfaz grafica(pueden ser apps por si solas o ser parte de apps existentes)

¿Que es JAVA?
R= Lenguaje de programacion y plataforma electronica. Lenguaje oop(programacion orientada a objetos)

¿Ejemplo basico de codigo java?
R= 
import java.util.Scanner;
 
public class sumarNumeros
{
    public static void main(String[] ARGS)
    {
        Scanner obtener = new Scanner(System.in);
        int cantidadNumeros,total,i,nuevoNumero;
 
        System.out.print("Ingrese la cantidad de numeros a sumar: ");
        cantidadNumeros = obtener.nextInt();
 
        total = 0;
 
        for(i = 1; i <= cantidadNumeros; i++)
        {
            System.out.print("Ingrese el numero (" + i + ") : ");
            nuevoNumero = obtener.nextInt();
 
            total = total + nuevoNumero;
        }
 
        System.out.print("La suma total de los numeros es : " + total);
    }
}


Ejemplo basico de codigo xml
R=
<menu_almuerzo>
    <comida>
        <nombre>Waffles</nombre>
        <precio>$2.00</precio>
        <descripcion>Waffles baratos de McDonalds</descripcion>
        <calorias>650</calorias>
    </comida>
    <comida>
        <nombre>Hamburguesa</nombre>
        <precio>$5.00</precio>
        <descripcion>La hamburguesa mas comun de McDonalds</descripcion>
        <calorias>1500</calorias>
    </comida>   
</menu_almuerzo>

Fuente: https://www.ejemplode.com/21-xml/525-ejemplo_de_menu_de_comidas_en_xml.html#ixzz6HYlKqDGs

Ejemplos de IDE de programacion para JAVA
R=
Net Beans. Funciona en base a modulos, con el puedes crear app web o para ordenadores y moviles.
Eclipse. (app web, ordenadores y moviles) caracteristicas y funciones: autocompletar codigos, explorador remoto del sistema, editor xml, gran libreria de plugins para extender las funciones.
Intellij idea. Desarrollo de apps android y lenguaje de programacion como Scala, Groovy, kotlin, etc.
Android Studio. Desarrollado por google basado en intellij IDEA.

¿Que es Android?
R=Sistema operativo movil desarrollado por google.

¿Que es un programa o un algoritmo?
R=es una formula para la resolucion de un problema, los algoritmos pueden ser ejecutados por un ser humano, los programas estan diseñados para ser ejecutados por maquinas. 

¿Que es un lenguaje de programacion?
R=es un lenguaje formal(con reglas gramaticales bien definidas)que proporciona a el programador, la capacidad de escribir(programar) un serie de instrucciones en forma de algoritmos con el fin de controlar el comportamiento fisico y/o logico de una computadora.

¿Que es lenguaje de alto nivel?
R=Se caracteriza por expresar los algoritmos de una manera adecuada a la capacidad cognitiva humana.

¿Que es un traductor?
R=Programa que tiene como entrada un texto escrito en un lenguaje(lenguaje fuente) y como salida un texto escrito en un lenguaje(lenguaje objeto)que precerva el significado de origen(ensambladores y compiladores).


¿Que es un compilador?
R=Programa que traduce un programa escritpo en un lenguaje de programacion a ptro lenguaje de programacion. El segundo lenguaje es un lenguaje de maquina, pero tambien puede ser un codigo intermedio(bytecode), o simplemente texto.

¿Que es JVM en programacion?
R=Java Virtual Machine(maquina virtual java) es un programa ejecutable en una plataforma especifica, capaz de interpretar y ejecutar instrucciones expresadas en un codigo binario especial(el java bytecode), el cual es generado por el compilador del lenguaje java. 

Ciclo para el desarrollo de aplicaciones moviles
R=
Tener una buena idea: Si ya tienes una idea para tu aplicación, puedes saltarte este punto. Si no es así, lo que realmente necesitas es buscar un problema o necesidad al que darle solución. 
 Planning: Las ideas y los planes de negocio están muy bien, pero la documentación adecuada es aún mejor. Los estudios de mercado, el desarrollo del MVP, el lanzamiento inicial, los cronogramas, el presupuesto… Tendrán que estar incluidos en tu plan de negocio, que revisarás cientos y cientos de veces.
Desarrollar la aplicación: es decir, crear el Wireframe (el proceso de crear una maqueta o prototipo de la aplicación). Debemos tener claro si queremos que nuestra app esté disponible para el sistema operativo Android, iOS o Windows Phone y si cumple los requisitos que estos sistemas imponen. En esta etapa del proceso también entra el diseño de la interfaz y la hoja de ruta con la que el usuario navegará a través de la app. Sería muy útil para el desarrollador que hicieras un boceto de los servidores, los APIs y los diagramas de datos.
Prototipo: construye un prototipo rápido para entender de primera mano lo que funciona y lo que no en tu app y así poder cambiarlo. No hace falta que esté todo perfecto, simplemente ofrece al usuario una experiencia similar a la que tendría con la app terminada para así tú obtener la retroalimentación que necesitas para poder continuar y finalizar tu proyecto de app
Testeo de la aplicación: hará falta tener listo un prototipo para poder probarlo con posibles usuarios de la app terminada. Haz que naveguen por tu wireframe y monitoriza todos sus pasos para saber qué necesitas mejorar para poder adaptar mejor tu app a los usuarios.
 Revisar y mejorar la app: una vez que hayas sometido tu futura app a todas las pruebas posibles y hayas realizado los cambios necesarios basándote en el feedback recogido en los “testeos”, deberás pulir la idea, el uso y el objetivo de tu aplicación. 
Lanzamiento y mantenimiento: tras haber publicado la aplicación terminada en plataformas tales como Apple Store o Play Store, proceso que puede llevarnos desde unas horas hasta unos días, dependiendo de si cumple o no las políticas regulatorias de cada sistema, debemos llevar a cabo su mantenimiento, lo que incluye actualizaciones, mejores y hasta el desarrollo de nuevas funcionalidades. También se ha de dar soporte técnico al cliente para solventar posibles errores o bugs que no se hayan detectado durante las pruebas.

¿Que son los operadores en programacion?
R=Simbolos que indican como se deben manipular los operandos .
Operandos, son datos que conecta y procesa el operador. operadores y operandos forman una expresion.

Tipos de datos primitivos en java
R=char, byte, short, int, long, float, double, boolean.

Operadores relacionales:
R=Los operadores relacionales son símbolos que se usan para comparar dos valores. 
<	menor que	        a<b	a es menor que b
>	mayor que	        a>b	a es mayor que b
==	igual a	a==b	        a es igual a b
!=	no igual a	        a!=b	a no es igual a b
<=	menor que o igual a	a<=5	a es menor que o igual a b
>=	mayor que o igual a	a>=b	a es menor que o igual a b

¿Que es una constante?
R=Es un valor que no puede ser alterado/modificado durante la ejecucion de un programa(valores fijos)

¿Que es una variable?
R=Esta formada por un espacio en el sistema de almacenaje y un nombre simbolico que esta asociado a dicho espacio.  


Tipos de operadores
R=
Operadores relacionales 
aritmeticos.-  *, /, %, +, -
unarios.-  -, +, ++, --
Asignación	                                           x = y	x = y
Asignación de adicción	                                  x += y	x = x + y
Asignación de sustracción	                          x -= y	x = x - y
Asignación de multiplicación	                          x *= y	x = x * y
Asignación de división	                                  x /= y	x = x / y
Asignación de Resto	                                  x %= y	x = x % y
Asignación de exponenciación	                         x **= y	x = x ** y
Asignación de desplazamiento a la izquierda	         x <<= y	x = x << y
Asignación de desplazamiento a la derecha	        x >>= y	        x = x >> y
Asignación sin signo de desplazamiento a la derecha	x >>>= y	x = x >>> y
Asignacion AND	    x &= y	x = x & y
Asignacion XOR	    x ^= y	x = x ^ y
Asignacion OR	    x |= y	x = x | y
logicos &, &&, ||
ternario.- if-else, if true, if false.
bit a  bit &, |, ^, ~.
shift.- 
Condiciones: (estructura)
R=Es toda sentencia de la cual se puede determinar su verdad(true)o falsedad(false). En su gran mayoria son comparaciones.

¿Que es un blucle?
R= Es una secuencia que ejecuta repetidas veces un trozo de código, hasta que la condición asignada a dicho bucle deja de cumplirse. Los tres bucles más utilizados en programación son el bucle while, el bucle for y el bucle do-while. 

¿Que es una funcion o un metodo en programacion?
R=Nos permite dividir el trabajo que hace un programa, en tareas mas pequeñas separadas de la parte principal.
