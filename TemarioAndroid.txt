PRE - Introduccion a Android.
    0.- Introducción (Framework crossplatform - Flutter, Xamarin, Reac-Native), precio y perfiles de desarrollo.
    0.1- https://flutter.dev
    0.2- https://reactnative.dev
    0.3- https://dotnet.microsoft.com/en-us/apps/xamarin
    1.- Que es XML y JSON 
    1.1- XML : https://oscarmaestre.github.io/lenguajes_marcas/tema5.html
    Ejercicios con XML
    1.2- JSON : https://www.freecodecamp.org/espanol/news/que-es-un-archivo-json-ejemplo-de-codigo-en-javascript/
    1.3 - https://jsoneditoronline.org/#right=local.rozuqa
    Repasaremos estos dos temas que ya vimos pero haremos incapie las diferencias entre cada uno y para que se usan en android.
    2.- Cómo hacer un diseño previo de tu app
    2.1- ninjaMock - https://ninjamock.com/home/index
    2.2- balsamiq - https://balsamiq.com
    Analizaremos la pagina ninjaMock y veremos un repaso rapido de esta.
    
Introduccion a Android.
    1.- Cómo instalar Android Studio
    Veremos la instalacion de Android Studio y todo los pasos previos
    para una correcta instalacion.
    2.- Cómo cambiar el tema de Android Studio
    Veremos como cambiar el tema de android studio
    3.- Cómo crear un Proyecto
    Crearemos un proyecto basico y veremos los componentes que lo integran.
    4.- Cómo crear dispositivos virtuales AVD
    Crearemos un dispositivo virtual desde cero.
    4.1.- Genymotion como alternativa
    Analizaremos Genymotion como se descarga como se instala y como se usa.
    5.- Cómo conectar Android Studio a tu teléfono o tablet
    Veremos las configuraciones principales que debe tener un dispositivo
    fisico para poder instalar una app desde el IDE
    6.- Entorno de desarrollo integrado Android Studio (IDE) Android Studio
    Explicacion sobre el IDE de android.
    7.- Estructura de un proyecto básico Android
    Veremos la estructura de un proyecto de android.
    8.- Hola Mundo
    Creacion y ejecucion del Hola mundo en android 
    9.- Debugging
    Analizaremos el Debugging en android y como se utiliza.
    10.- Activity, Context y Ciclos de Vida de Activities
    10.1.- https://developer.android.com/reference/android/app/Activity
    11. Referencias en Android
    11.1.-https://developer.android.com/guide/topics/resources/accessing-resources?hl=es-419
    12.- Librerías de terceros mediante Gradle
    12.1.- https://material.io
    12.1.- https://android-arsenal.com



Diseñando Android
    1.- Layouts
    1.1.- LinearLayout vertical
    1.2.- LinearLayout vertical full
    1.3.- LinearLayout horizontal 
    1.4.- LinearLayout horizontal full
    1.5.- RelativeLayout
    1.6.- TableLayout
    1.7.- GridLayout
    1.8.- FrameLayout
    1.9.- ConstraintLayout
    1.1.1.- ScrollView
    1.1.2.- WebView

    2.- Elementos de la UI
    2.1.- TextView
    2.2.- EditText
    2.3.- Button
    2.4.- ImageView
    2.5.- CheckBox
    2.6.- RadioButtons
    2.7.- Switch
    3.- Propiedades de elementos de la UI
    4.- Añadir Funcionalidad a elementos UI
    5.- Contexto de aplicacion y Contexto de Acitividad.
    6.- Intent explicito con datos
    6.1.- https://developer.android.com/guide/components/intents-filters
    7.- Intent implícito 
    9.- Pasar parametros con intents 
    8.- Icono, Nombre App y Up Button

* PROYECTO "Diseñando Android" - Menu de operaciones.
https://bitbucket.org/Dalvik31/curso-android/src/master/Practicas/

8

ListView y GridView
    0.- Spinner ComboBox.
    1.- Simple ListView
    2.- Simple ListView - Click

* Proyecto ListView y GridView (Intermedio) - Curriculum
https://bitbucket.org/Dalvik31/curso-android/src/master/Proyectos/Curriculum/
https://ninjamock.com/Designer/Workplace/124842967/Page4

    2.1.- Objetos (POJO), Constuctores, getters y setters
    3.- ListView personalizado layout 
    4.- ListView personalizado basico adaptador
    5.- ViewHolder pattern en Adapter para listView
    6.- GridView
    7.- Botón Option Menu (Menu de opciones)
    7.1.- https://developer.android.com/guide/topics/ui/menus?hl=es-419
    8.- Context Menu (Menu de opciones como mensaje)

* Proyecto Proyecto ListView y GridView - Lista de libros
https://bitbucket.org/Dalvik31/curso-android/src/master/Proyectos/Biblioteca/
* PROYECTO "Reserva de hoteles" - (Final) 
https://bitbucket.org/Dalvik31/curso-android/src/master/Proyectos/Happy_Holidays/

CardView y RecyclerView
    1.- RecyclerView
    1.1.-https://developer.android.com/guide/topics/ui/layout/recyclerview?hl=es
    2.- RecyclerView - List y Grid 
    3.- CardView
    3.1.- https://developer.android.com/guide/topics/ui/layout/cardview
    4.- CardView - Ripple
    5.- Recycler View + Card View
    6.- Interface Click

* Proyecto con recycler view (Hoteles)
https://bitbucket.org/Dalvik31/curso-android/src/master/Practicas/Hoteles_Recycler_View/

Shared Preferences y SplashScreen
    1.- Conceptos
    1.1.- https://developer.android.com/training/data-storage/shared-preferences?hl=es
    1.2.- Crear context de aplicacion.
    2.- Modificando valores a Shared Preference
    3.- Creando Login
    4.- Validando Login
    5.- Cerrar sesión, borrar y leer Shared Preferences
    6.- Implementando SplashScreen 
    6.1.- https://medium.com/@sneyderangulo/como-implementar-splash-screen-correctamente-en-android-f6abcc592b4c
    7.- SplashScreen como activity, para la carga de algun servicio.
    7.- SplashScreen + Shared Preferences

* Agregar Splash a los proyectos que hemos realizado 
* Dividir el proyecto Happy_Holidays en pasos, y guardarlos es SharedPreferences y si el usuario cierra la aplicación  saber en que paso se quedo y cuando inicie la app mandarlo a ese paso.

Fragments
    1.- Conceptos
    1.1.- https://developer.android.com/guide/components/fragments?hl=es-419
    2.- Añadiendo fragments al proyecto
    3.- Configurando fragments
    4.- Modificando layouts del fragment
    5.- Añadiendo archivo arrays.xml
    6.- Crear folder layout-land, para cuando el dispositivo se encuentre de modo lanscape
    7.- Agregando validaciones.

    Actividad de fragmentos
    Pasar el proyecto de hoteles a fragmentos 
    * cuando el dispositivo se encuentre en estado Portrait el usuario vera la lista y cuando de clic lo mandara a la vista del detalle
    * Cuando el dispositivo se encuentre en landscape el usuario vera la vista de lista y la vista de detalle juntas!

Fechas
    0.- Epoch
    0.1.- https://www.epochconverter.com
    1.- Conceptos
    1.1.- https://developer.android.com/guide/topics/ui/controls/pickers
    2.- Seleccionar Fechas
    3.- Seleccionar hora
    4.- Material DatePicker

Actividad fechas
    Agregar selector de fechas a proyecto hotels.

Toolbar
    1.- Conceptos
    1.1.- https://developer.android.com/training/appbar/setting-up?hl=es-419
    2.- Toolbar como layout
    3.- Toolbar con titulo y subtitulo 
    4.- Toolbar con titulo y subtitulo y menu
    5.- Toolbar con imagen
    6.- Conceptos material toolbar
    6.1.- https://material.io/develop/android/components/app-bars-top
    7.- material toolbar con imagen y toolbar
    8.- Eliminando toolbar del proyecto.
    9.- Agregando toolbar a supportactionbar
    10.- Collapsing toolbar
Actividad toolbar
    Agregar un toolbar global al proyecto de hoteles.
    Agregar un collapsing toolbar al detalle del hotel.

 ViewPager and Tabs
    1.- Conceptos tabs y Viewpager
    1.1.-https://developer.android.com/guide/navigation/navigation-swipe-view?hl=es-419
    2.- Creando Fragments para la vista de Profile, List y Search
    3.- Modificando layout agregando Tablayout y Viewpager
    4.- Creando adapter para Viewpager
    5.- Agregando adapter a tab y view pager

Permisos en tiempo de ejecucion
    1.- Conceptos
    1.1.- https://developer.android.com/training/permissions/requesting
    2.- SnackBar Conceptos
    2.1.- https://developer.android.com/training/snackbar
    3.- Agregando permiso en Manifest
    4.- Agregando validacion en activity.

Actividad para permisos.
    * Después del Splash crear una vista con un viewpager donde se pidan los permisos necesarios para poder ejecutar la app,
     algo como un tutoríal.

Maps
    1.- Conceptos
    1.0.-https://developers.google.com/maps/documentation/android-sdk/start
    1.1.-https://developers.google.com/maps/documentation/android-sdk/intro
    2.- Creando proyecto maps desde cero.
    3.- Creando api key debug
    4.- Creando api key release
    5.- Corriendo app en release
    6.- Configurando proyecto en release y debug
    7.- Pruebas para correr app en debug y en release.
    8.- Agregando mapa a una app
    8.1.- https://developers.google.com/maps/documentation/android-sdk/map-with-marker
    9.- Creando carpetas para debug y release.
    10.- Creando archivo google maps key para debug y release.
    11. Creando archivos para debug y release
    12.- Modificando proyecto para ejecutarlo en modo release.
    13.- restringiendo clave release y debug.
    14.- probando implementacion

Actividad para mapas.
* Al proyecto de happyholiday agregar la funcionalidad de mapas, crear un nuevo valor en el objeto hoteles 
para poder agregar la latitud y longitud y con esos datos poder pintar en un mapa el marcador con la ubicación. 
En la vista detalle  del hotel agregar una opción para llevarte al mapa.

MVP
    1.- Conceptos
    1.1.- https://devexperto.com/mvp-android/
    1.2.- https://1.bp.blogspot.com/-lAxfZoS-ncE/XTj0P4iJWkI/AAAAAAAABbs/E9-O3gM803oGp6gmuQXoZiJhqBQj2vUeACLcBGAs/s1600/1*p2JvbgEir0BusDiiVHMvIA.png
    2.- Video tutorial.
    2.- https://www.youtube.com/watch?v=CeRnCgoG1N4

Actividad para MVP
* Seguir el tutorial. 

API
    1.- Conceptos
    1.1.- https://geekytheory.com/que-es-una-api-rest-y-para-que-se-utiliza
    e.- Analizando url con response JSON
    3.1.- https://rickandmortyapi.com/api/character/
    2.- Analizando Movie db
    2.1.- https://www.themoviedb.org/?language=es
    3.- Analizando Marvel API
    3.1.- https://developer.marvel.com
    a.- Analizando jsonschema2pojo
    a.1.- http://www.jsonschema2pojo.org/

Actividad
* Agregar zoom a mapa
* Buscar api diferente y hacer pruebas.

Retrofit
    4.- Retrofit 
    4.1.- Conceptos
    4.2.- http://blog.fixter.org/retrofit-el-mejor-cliente-rest/
    4.3.- https://square.github.io/retrofit/
    4.4.- Volley https://developer.android.com/training/volley
    5.- Ejemplo basico con Retrofit.
    6.- Ejemplo Interceptor con Retrofit.
    5.- Ejemplo Session con Retrofit.
    5.- Ejemplo Time con Retrofit.
    5.- Ejemplo Custom con Retrofit.
    5.- Ejemplo Custom layout y lottie con Retrofit.
    5.- Ejemplo MVP Retrofit.
    7.-Url para recibir un codigo de respuesta statico y un tiempo determinado 
    7.-https://httpstat.us
    7.- Uso http://httpstat.us/404?sleep=5000
    8.-Lottie tutorial
    8.-1- https://heartbeat.fritz.ai/using-lottie-on-android-to-implement-animations-in-your-apps-adbdaf73ec67
    9.-lottie animation
    9.1.- https://lottiefiles.com/featured


Actividad
    * Agregar Dialogs cuanto sea posible.
    * Crear una aplicacion que utilice Retrofit para descargar informacion de las api investigadas con MVP.*
    * Utilizar lottie *
    * Utilizar custom y progressdialog.
    * Crear una segunda aplicacion que utilice Volley para descargar informacion con MVP. (Solo conexion y descarga de informacion)
    *Utilizar gson, jsonschema2pojo, ocupar interceptor para el apikey*
    * Crear lista customizada con la respuesta.*
    * Crear una vista con el detalle del elemento seleccionado.*



    





 








